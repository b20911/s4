const {assert} = require('chai');
const {getCircleArea, getNumberOfChar} = require('../src/util.js')

describe('test_get_circle_area',()=>{

    it('test_area_of_circle_radius_15_is_706.86',()=>{

        let area = getCircleArea(15);
        assert.equal(area,706.86);

    })

    it('test_area_of_circle_neg1_radius_is_undefined',()=>{

        let area = getCircleArea(-1);
        assert.isUndefined(area);

    })

    it('test_area_of_circle_0_radius_is_undefined',()=>{
        let area = getCircleArea(0);
        assert.isUndefined(area);
    })

    it('test_area_of_circle_radius_is_not_a_number_is_undefined',()=>{
        let area = getCircleArea("hi");
        assert.isUndefined(area)
    })

    it('test_area_of_circle_invalid_number_is_undefined',()=>{
        let area = getCircleArea({number:25});
        assert.isUndefined(area);
    })
})

describe('test_get_number_of_char_in_string',()=>{

    it('test_number_of_l_in_string_is_3',()=>{

        let numChar = getNumberOfChar("l","Hello World");
        assert.equal(numChar,3);
    })

    it('test_number_of_a_in_string_is_2',()=>{
        let numChar = getNumberOfChar("a","Malta");
        assert.equal(numChar,2);
    })

    it('test_number_of_char_if_first_argument_is_not_a_string_is_undefined',()=>{
        let numChar = getNumberOfChar(5,"Monitor");
        assert.equal(numChar,undefined)
    })

    it('test_number_of_char_if_second_argument_is_not_a_string_is_undefined',()=>{
        let numChar = getNumberOfChar("five",5);
        assert.equal(numChar,undefined)
    })
})